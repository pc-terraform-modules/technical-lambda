# kafka-config


Run and build with : 
- python-3.7
- confluent-kafka 1.6.0
- librdkafka >= 1.6.0 (latest release is embedded in wheels)


Useful documentation:

####More about Partitons
https://www.confluent.io/blog/how-choose-number-topics-partitions-kafka-cluster/
https://blog.softwaremill.com/7-mistakes-when-using-apache-kafka-44358cd9cd6
####Definitive guide 

https://github.com/jitendra3109/ApacheKafka/blob/master/Docs/confluent-kafka-definitive-guide-complete.pdf
####Stackoverflow article 
https://stackoverflow.com/questions/38024514/understanding-kafka-topics-and-partitions


Before creating topics in Kafka, please check what would be the best fit for your use case.
``Important!!!`` scaling up number of partitions per topic i possible, while scaling down is not possible, please check documentation if you are not sure how to configure yout topic.

 

In ``DEV / STAGE`` we create Kafka Clusters of 2 nodes, thats why you should set ``replicas`` 2(if you need backup)

In `` PROD`` we assume that minimal Kafka cluster will be 3 nodes big


- ####DEV/STAGE Example config file for Kafka 
```buildoutcfg
{
  "topics": [
    {
      "topic": "test1",
      "partitions": 2,
      "replicas": 2
    },
    {
      "topic": "test2",
      "partitions": 2,
      "raplicas": 2
    },
    {
      "topic": "test3",
      "partitions": 2,
      "replicas": 2
    }
  ]
}


```

- ####PROD Example config file for Kafka 
```buildoutcfg
{
  "topics": [
    {
      "topic": "test1",
      "partitions": 3,
      "replicas": 3
    },
    {
      "topic": "test2",
      "partitions": 3,
      "raplicas": 3
    },
    {
      "topic": "test3",
      "partitions": 3,
      "replicas": 3
    }
  ]
}


```
# postgres-config

Run and build with : 
- python-3.7
- psycopg2
- boto3


This scrip is prepared to create initial setup in Postgres Database, which includes steps like :
1. Create database
2. Create role
3. Create user
4. Add user password to ssm 
5. Update initial root password of RDS instance

### **SSM structure:**

eg path to get user password : /rds/instance-name/db-name/username
``` 

-rds----|
        |
        |instance-|
        |         |
        |         |-db-name-|
        |         |         |
        |         |         |-username
        |         |         |
        |         |         |-username2
        |         |
        |         |        
        |         |-db-name2-|
        |                    |-username
        |
        |
        |instance2-|
                   |
                   |-db-name
                   
```

Script will create database and 3 pre-defined default role: 

-   **RW_ROLE:** this role has permission to read and write in database
-   **RO_ROLE:** this role has permission to read only from database
-   **MIGRATOR_ROLE:** has full access to database(root)

Config file example

```buildoutcfg
[
   {
      "Database":"frostapp",           <<<< Database name 
      "Users":[
         {
            "Name":"frost_user_app",   <<<< Username 
            "Role":"rw_role"           <<<< Role
         },
         {
            "Name":"frost_user_migrator",
            "Role":"migrator_role"
         },
         {
            "Name":"frost_user_ro",
            "Role":"ro_role"
         }
      ]
   }
]
```